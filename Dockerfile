################## BEGIN INSTALLATION ######################
FROM ubuntu:16.04
# Install general dependencies
RUN apt update
RUN apt install -y \
	wget \
	apt-utils \
	sudo \
	psmisc

# Install dependecies
RUN apt install -y \
	autoconf \
	automake \
	bison \
	build-essential \
	cmake \
	cmake-curses-gui \
	doxygen \
	doxygen-gui \
	flex \
	gccxml \
	gdb \
	git \
	pkg-config \
	subversion
# Install libraries
RUN apt install -y \
	guile-2.0-dev \
	libconfig8-dev \
	libgcrypt11-dev \
	libgmp-dev \
	libhogweed? \
	libgtk-3-dev \
	libidn2-0-dev \
	libidn11-dev \
	libpthread-stubs0-dev \
	libsctp1 \
	libsctp-dev \
	libssl-dev \
	libtool \
	libxml2 \
	libxml2-dev \
	mscgen \
	openssl \
	check \
	python

# dependencies for gnutls
RUN apt install -y \
	autogen \
	pkg-config

# remove_nettle_from_source
RUN apt remove -y nettle-dev nettle-bin
WORKDIR /tmp
RUN rm -rf nettle-2.5.tar.gz* nettle-2.5
RUN wget https://ftp.gnu.org/gnu/nettle/nettle-2.5.tar.gz
RUN tar -xzf nettle-2.5.tar.gz
WORKDIR /tmp/nettle-2.5/
RUN ./configure --disable-openssl --enable-shared --prefix=/usr
RUN make uninstall || true
WORKDIR /

# Install nettle library
RUN apt install -y \
	nettle-dev \
	nettle-bin

# remove_gnutls_from_source
RUN apt remove -y libgnutls-dev
WORKDIR /tmp
RUN echo "Downloading gnutls archive"
RUN rm -rf /tmp/gnutls-3.1.23.tar.xz* /tmp/gnutls-3.1.23
RUN wget http://mirrors.dotsrc.org/gcrypt/gnutls/v3.1/gnutls-3.1.23.tar.xz
RUN tar -xJf gnutls-3.1.23.tar.xz
WORKDIR /tmp/gnutls-3.1.23/
RUN apt install -y nettle-dev nettle-bin && ./configure --prefix=/usr
RUN make uninstall || true
WORKDIR /

# install libgnutls
RUN apt install -y libgnutls-dev

#Install FreeDiameter
RUN apt install -y \
	debhelper \
	g++ \
	libgcrypt-dev \
	libgnutls-dev \
	libpq-dev \
	libxml2-dev \
	mercurial \
	python-dev \
	ssl-cert \
      	swig
WORKDIR /tmp
RUN echo "Downloading 1.2.0 freeDiameter archive"
RUN rm -rf 1.2.0.tar.gz* freeDiameter-1.2.0 freediameter
RUN GIT_SSL_NO_VERIFY=true git clone https://gitlab.eurecom.fr/oai/freediameter.git -b eurecom-1.2.0
RUN mkdir freediameter/build
WORKDIR /tmp/freediameter/build
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local ../
RUN echo "Compiling freeDiameter"
RUN make -j`nproc`
RUN make install
RUN rm -rf /tmp/1.2.0.tar.gz /tmp/freeDiameter-1.2.0 /tmp/freediameter
WORKDIR /

# install_asn1c_from_source
RUN rm -rf /tmp/asn1c
RUN mkdir -p /tmp/asn1c
RUN git clone https://gitlab.eurecom.fr/oai/asn1c.git /tmp/asn1c
WORKDIR /tmp/asn1c
RUN ./configure
RUN make install
WORKDIR /


# install_libgtpnl_from_source
RUN apt install -y libmnl-dev
WORKDIR /tmp
RUN echo "Downloading libgtpnl archive"
RUN rm -rf /tmp/libgtpnl*
RUN git clone git://git.osmocom.org/libgtpnl
WORKDIR /tmp/libgtpnl
RUN autoreconf -fi
RUN ./configure
RUN echo "Compiling nettle"
RUN make -j`nproc`
RUN make install
RUN ldconfig
WORKDIR /tmp
RUN rm -rf /tmp/libgtpnl*
WORKDIR /

# Install network tools
RUN apt install -y \
	ethtool \
	iproute \
	vlan \
	iptables

#
RUN apt install -y \
	check \
	python-pexpect \
	unzip

################## END INSTALLATION ######################

################## INSTALL wait-for script ################
RUN git clone https://github.com/Eficode/wait-for.git
################## END INSTALL wait-for script ################

################## BEGIN CONFIGURING OAI ######################
# Create configuration folder
RUN rm -rf /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai/freeDiameter

# Copy OpenAir Interface configuration files
COPY oai_conf/mme.conf /usr/local/etc/oai/mme.conf
COPY oai_conf/freeDiameter /usr/local/etc/oai/freeDiameter


# Clone OpenAir Interface
RUN git clone -b develop https://gitlab.eurecom.fr/oai/openair-cn.git

# Build MME
WORKDIR /openair-cn/scripts
RUN ./build_mme --clean

CMD ./run_mme
################## END CONFIGURING OAI ######################
